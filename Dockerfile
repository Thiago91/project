FROM ubuntu:16.04

LABEL Author="Esron Silva esron.silva@sysvale.com"

ARG USER_ID

ENV PATH ${PATH}:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin

RUN apt-get update \
	&& apt-get upgrade -y \
	&& apt-get install -y software-properties-common \
		unzip \
		apache2 \
		curl \
		language-pack-en-base

# Add PHP ondrej repository
RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php \
	&& apt-get update && apt-get upgrade -y

# Install php and its libraries
RUN apt-get -y install php7.2 \
	php7.2-mbstring \
	php7.2-xml \
	php7.2-curl \
	php7.2-mysql \
	php7.2-imagick \
	php7.2-zip \
	libxrender1 \
	libfontconfig1 \
	libxtst6 \
	&& update-alternatives --set php /usr/bin/php7.2

# Apache config
RUN a2enmod rewrite
ADD apache.conf /etc/apache2/sites-enabled/000-default.conf

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&& php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
	&& php -r "unlink('composer-setup.php');"

COPY . /app

WORKDIR /app

RUN useradd -m -G www-data,adm -u ${USER_ID:-1000} laravel-user \
	&& chgrp www-data bootstrap/cache storage /etc/apache2 /var/www /var/lock/apache2 /var/run/apache2 -R \
	&& chmod g+w -R bootstrap/cache storage /etc/apache2 /var/www /var/log/apache2 /var/lock/apache2 /var/run/apache2 \
	&& find . -type d ! -path './vendor/symfony/finder/Tests/Fixtures/with*' ! -path 'space*' | xargs chmod g+w

USER laravel-user

CMD ["/app/entrypoint.sh"]
